import {createStore, applyMiddleware} from 'redux'
import rootReducers from '../reducers/rootReducers'
import {createEpicMiddleware} from 'redux-observable'
import {rootEpic} from '../epic'

const epicMiddleware = createEpicMiddleware()

export default function configureStore() {
  const store = createStore(
    rootReducers,
    applyMiddleware(epicMiddleware)
  )

  epicMiddleware.run(rootEpic)

  return store
}
