import {constants} from '../vars/constants'

export const fetchData = () => ({
  type: constants.FETCH_DATA,
  isLoading: true
})

export const fetchDataSuccess = (task) => ({
  type: constants.FETCH_DATA_SUCCESS,
  payload: task
})

export const changeTaskSuccess = task => ({
  type: constants.CHANGE_TASK_SUCCESS,
  payload: task
})

export const fullDeleteTaskSuccess = taskId => ({
  type: constants.FULL_DELETE_TASK_SUCCESS,
  taskId: taskId
})

export const preparingEdit = (taskId, taskName) => ({
  type: constants.PREPARE_EDIT,
  taskId: taskId,
  taskName: taskName
})

//action dispatch
export const addTask = taskName => ({
  type: constants.ADD_TASK,
  taskName: taskName
})

export const editTask = (taskId, taskName) => ({
  type: constants.EDIT_TASK,
  taskId: taskId,
  taskName: taskName
})

export const toggleTask = (taskId, taskDone) => ({
  type: constants.TOGGLE_TASK,
  taskId: taskId,
  taskDone: taskDone
})

export const deleteTask = taskId => ({
  type: constants.DELETE_TASK,
  taskId: taskId
})

export const unDeleteTask = taskId => ({
  type: constants.UNDELETE_TASK,
  taskId: taskId
})

export const fullDeleteTask = taskId => ({
  type: constants.FULL_DELETE_TASK,
  taskId: taskId
})