import React from 'react'
import {createStackNavigator, createAppContainer} from 'react-navigation'
import HomeScreen from '../screens/Home/HomeScreen'
import ModalScreen from '../screens/Modal/ModalScreen'
import {Fade} from '../asset/anim/Fade'

const MainStack = createStackNavigator(
  {
    Main: HomeScreen,
    Modal: {
      screen: ModalScreen,
      navigationOptions: () => ({
        gesturesEnabled: false,
      }),
    }
  },
  {
    mode: 'modal',
    headerMode: 'none',
    transparentCard: true,
    cardStyle: {
      backgroundColor: 'transparent',
      opacity: 1,
    },
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: 'transparent',
      },
      screenInterpolator: Fade,
    })
  }
)

export const MainNavigator = createAppContainer(MainStack)