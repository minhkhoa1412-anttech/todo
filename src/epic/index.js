import {fetchDataSuccess, changeTaskSuccess, fullDeleteTaskSuccess} from '../actions'
import {ofType} from 'redux-observable'
import {map, mergeMap} from 'rxjs/operators'
import firebase from 'firebase'
import {combineEpics} from 'redux-observable'
import {constants} from '../vars/constants'
import {Observable} from 'rxjs'

const addListener$ = () => {
  return new Observable(observer => {
    firebase.database().ref('tasks').on('child_added', (snapshot) => {
      observer.next(snapshot.val())
    })
  })
}

const fetchDataEpic = action$ => action$.pipe(
  ofType(constants.FETCH_DATA),
  mergeMap(() => {
    return addListener$().pipe(
      map(value => {
        return fetchDataSuccess(value)
      })
    )
  })
)

const changeListener$ = () => {
  return new Observable(observer => {
    firebase.database().ref('tasks').on('child_changed', (snapshot) => {
      observer.next(snapshot.val())
    })
  })
}

const changeTaskEpic = action$ => action$.pipe(
  ofType(constants.FETCH_DATA),
  mergeMap(() => {
    return changeListener$().pipe(
      map(value => {
        return changeTaskSuccess(value)
      })
    )
  })
)

const removeListener$ = () => {
  return new Observable(observer => {
    firebase.database().ref('tasks').on('child_removed', (snapshot) => {
      observer.next(snapshot.val())
    })
  })
}

const fullDeleteTaskEpic = action$ => action$.pipe(
  ofType(constants.FETCH_DATA),
  mergeMap(() => {
    return removeListener$().pipe(
      map(value => {
        return fullDeleteTaskSuccess(value.taskId)
      })
    )
  })
)

//epic has action dispatch
const addTask = action$ => action$.pipe(
  ofType(constants.ADD_TASK),
  mergeMap((action) => {
    const task = {
      taskId: firebase.database().ref().push().key,
      taskDeleted: false,
      taskDone: false,
      taskDate: Date.now(),
      taskName: action.taskName
    }
    firebase.database().ref('tasks/' + `${task.taskId}`).set(task).then(() => {})
    return []
  })
)

const editTask = action$ => action$.pipe(
  ofType(constants.EDIT_TASK),
  mergeMap((action) => {
    firebase.database().ref('tasks/' + `${action.taskId}/` + 'taskName').set(action.taskName).then(() => {})
    return []
  })
)

const toggleTask = action$ => action$.pipe(
  ofType(constants.TOGGLE_TASK),
  mergeMap((action) => {
    firebase.database().ref('tasks/' + `${action.taskId}/` + 'taskDone').set(!action.taskDone).then(() => {})
    return []
  })
)

const deleteTask = action$ => action$.pipe(
  ofType(constants.DELETE_TASK),
  mergeMap((action) => {
    firebase.database().ref('tasks/' + `${action.taskId}/` + 'taskDeleted').set(true).then(() => {})
    return []
  })
)

const unDeleteTask = action$ => action$.pipe(
  ofType(constants.UNDELETE_TASK),
  mergeMap((action) => {
    firebase.database().ref('tasks/' + `${action.taskId}/` + 'taskDeleted').set(false).then(() => {})
    return []
  })
)

const fullDeleteTask = action$ => action$.pipe(
  ofType(constants.FULL_DELETE_TASK),
  mergeMap((action) => {
    firebase.database().ref('tasks/' + `${action.taskId}`).remove().then(() => {})
    return []
  })
)

export const rootEpic = combineEpics(
  fetchDataEpic,
  changeTaskEpic,
  fullDeleteTaskEpic,
  //epic has action dispatch
  addTask,
  editTask,
  toggleTask,
  deleteTask,
  unDeleteTask,
  fullDeleteTask
)