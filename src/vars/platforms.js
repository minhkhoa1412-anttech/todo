import { Platform } from 'react-native'

export const platforms = {
  isAndroid: Platform.OS === 'android',
}
