export const strings = {
  success: 'SUCCESS',
  onProgress: 'ON PROGRESS',
  addTask: 'Add your task',
  saveTask: 'SAVE',
  choseOption: 'Chose option',
  edit: 'Edit',
  done: 'Done',
  delete: 'Delete',
  undelete: 'Undelete',
  undo: 'Undo',
  cancel: 'Cancel',
  emptyTrash: 'Yours trash is empty'
}

export const months = {
  january: 'Jan',
  february: 'Feb',
  march: 'Mar',
  april: 'Apr',
  may: 'May',
  june: 'Jun',
  july: 'Jul',
  august: 'Aug',
  september: 'Sep',
  october: 'Oct',
  november: 'Nov',
  december: 'Dec'
}