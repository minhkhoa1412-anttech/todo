import React,{Component} from 'react'
import {Provider} from 'react-redux'

import configureStore from './store'
import {MainNavigator} from './navigator/MainNavigator'

class Application extends Component {
  componentDidMount() {
  }

  render() {
    return (
      <Provider store={configureStore()}>
        <MainNavigator/>
      </Provider>
    )
  }
}

export default Application