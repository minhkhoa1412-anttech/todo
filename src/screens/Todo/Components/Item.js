import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {colors} from '../../../vars/colors'
import {metrics} from '../../../vars/metrics'
import {strings, months} from '../../../vars/strings'
import moment from 'moment'
import ATag from '../../../components/ATag/ATag'
import Ripple from 'react-native-material-ripple'
import * as Animatable from 'react-native-animatable'

class Item extends Component {

  parsMonthNumberToString = (month) => {
    switch (month) {
      case 0:
        return months.january
      case 1:
        return months.february
      case 2:
        return months.march
      case 3:
        return months.april
      case 4:
        return months.may
      case 5:
        return months.june
      case 6:
        return months.july
      case 7:
        return months.august
      case 8:
        return months.september
      case 9:
        return months.october
      case 10:
        return months.november
      case 11:
        return months.december
    }
  }

  render() {
    const {item, index, showPopup} = this.props

    return (
      <View style={styles.container}>

        <Animatable.View
          animation='zoomIn'
          duration={600}
          delay={200}
          style={styles.containDot}>
          <View>
            <View style={[styles.dot, {marginTop: index === 0 ? 50 : 30}]}>
              <View
                style={[
                  styles.dotInner,
                  {backgroundColor: item.taskDone ? colors.colorDotComplete : colors.colorDotNotComplete}
                ]}
              />
            </View>
          </View>
        </Animatable.View>

        <Animatable.View
          animation='zoomIn'
          duration={600}
          delay={200}
          style={[styles.containTask, {paddingTop: index === 0 ? 20 : 0}]}>

          <Ripple
            rippleDuration={500}
            onPress={() => {
              setTimeout(() => {
                showPopup(item)
              }, 400)
            }}
          >
            <View style={styles.information}>
              <View style={styles.dateTime}>
                <Text style={styles.textMonth}>{this.parsMonthNumberToString(moment(item.taskDate).month())}</Text>
                <Text style={styles.textDate}>{moment(item.taskDate).date()}</Text>
              </View>

              <View style={styles.separated}/>

              <View style={styles.task}>
                <Text
                  numberOfLines={2}
                  style={[styles.textDate, styles.textTaskName]}
                >{item.taskName}</Text>
                <View style={styles.viewTimeAndProgress}>
                  <ATag
                    style={[
                      styles.tag,
                      {backgroundColor: item.taskDone ? colors.colorDotComplete : colors.colorDotNotComplete}
                    ]}
                    styleLabel={styles.textTime}
                    label={moment(item.taskDate).format('LT')}/>
                  <Text
                    style={[styles.textMonth, styles.textProgress]}>{item.taskDone ? strings.success : strings.onProgress}
                  </Text>
                </View>
              </View>
            </View>
          </Ripple>

        </Animatable.View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: metrics.screen_width
  },
  dot: {
    shadowColor: colors.colorShadow,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    backgroundColor: 'white',
    width: 15,
    height: 15,
    borderRadius: 7.5,
    margin: 25,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dotInner: {
    backgroundColor: colors.colorBlack,
    width: 10,
    height: 10,
    borderRadius: 5
  },
  containDot: {
    backgroundColor: 'transparent'
  },
  containTask: {
    flex: 1,
    backgroundColor: colors.colorBackground,
    paddingRight: 20,
    paddingBottom: 20,
  },
  information: {
    shadowColor: colors.colorShadow,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 1,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 4,
    flexDirection: 'row'
  },
  dateTime: {
    marginLeft: 7,
    marginRight: 7 + 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textMonth: {
    color: colors.colorTimeline,
    fontSize: 15
  },
  textDate: {
    color: colors.colorTextTitle,
    marginTop: 3,
    fontSize: 25
  },
  separated: {
    backgroundColor: colors.colorTimeline,
    width: 1
  },
  task: {
    justifyContent: 'center',
    marginHorizontal: 7 + 15,
    marginRight: 7 + 15 + 20 + 7 + 15
  },
  textTaskName: {
    fontSize: 16,
    marginBottom: 4
  },
  textProgress: {
    fontSize: 14
  },
  viewTimeAndProgress: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  textTime: {
    fontSize: 10
  },
  tag: {
    padding: 2,
    paddingHorizontal: 3,
    marginRight: 4
  }
})

export default Item
