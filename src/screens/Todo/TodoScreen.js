import React, {Component} from 'react'
import {View, StyleSheet, FlatList, Alert} from 'react-native'
import {connect} from 'react-redux'
import * as Animatable from 'react-native-animatable'

import {colors} from '../../vars/colors'
import Item from './Components/Item'
import {strings} from '../../vars/strings'
import {preparingEdit, fetchData, toggleTask, deleteTask} from '../../actions'
import firebase from 'firebase'
import {config} from '../../database/firebaseConfig'

class TodoScreen extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    firebase.initializeApp(config)
    this.props.fetchTask()
  }

  deleteTask = (item) => {
  }

  showDialogConfirmation = (item) => {
    Alert.alert(
      strings.choseOption,
      item.taskName,
      [
        {
          text: strings.cancel,
          style: 'cancel',
        },
        {
          text: strings.edit,
          onPress: () => {
            this.props.prepareEditTask(item.taskId, item.taskName)
            this.props.screenProps.navigate('Modal')
          }
        },
        {
          text: strings.delete,
          onPress: () => {
            this.props.deleteTask(item.taskId)
          }
        },
        {
          text: item.taskDone ? strings.undo : strings.done,
          onPress: () => {
            this.props.toggleTask(item.taskId, item.taskDone)
          }
        }
      ],
      {
        cancelable: true,
      },
    )
  }

  getTask = (tasks) => (
    tasks.filter(task => (
      !task.taskDeleted
    ))
  )

  render() {
    const {tasks, isLoading} = this.props

    return (
      <View style={styles.container}>

        <Animatable.View
          style={[styles.timeline, {backgroundColor: this.props.tasks.length > 0 ? colors.colorTimeline : colors.colorBackground}]}
          animation='bounceInDown'
          duration={6000}
          delay={1000}>
          <View />
        </Animatable.View>

        <FlatList
          refreshing={isLoading}
          onRefresh={this.props.fetchTask}
          keyExtractor={(item) => item.taskId}
          style={styles.list}
          data={this.getTask(tasks)}
          renderItem={({item, index}) => (
            <Item
              showPopup={this.showDialogConfirmation}
              index={index}
              item={item}
            />
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.colorBackground
  },
  list: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  },
  timeline: {
    height: '100%',
    width: 1,
    backgroundColor: colors.colorBackground,
    marginLeft: 32
  }
})

const mapStateToProps = (state) => {
  return {
    tasks: state.todos.tasks,
    edit: state.todos.editTask,
    isLoading: state.todos.isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTask: () => {
      dispatch(fetchData())
    },
    prepareEditTask: (id, name) => {
      dispatch(preparingEdit(id, name))
    },
    toggleTask: (id, done) => {
      dispatch(toggleTask(id, done))
    },
    deleteTask: id => {
      dispatch(deleteTask(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoScreen)