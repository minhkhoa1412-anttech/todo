import React, {Component} from 'react'
import {View, Text, StyleSheet, FlatList, Alert} from 'react-native'
import Animation from 'lottie-react-native'
import {connect} from 'react-redux'

import {metrics} from '../../vars/metrics'
import {strings} from '../../vars/strings'
import {colors} from '../../vars/colors'
import empty from '../../asset/anim/empty_box'
import Item from '../../screens/Todo/Components/Item'
import {unDeleteTask, fullDeleteTask} from '../../actions'

class TodoDoneScreen extends Component {
  componentDidMount() {
    // if (this.getTask(this.props.tasks) !== null) {
    //   this.animation.play()
    // }
  }

  getTask = (tasks) => (
    tasks.filter(task => (
      task.taskDeleted
    ))
  )

  showDialogConfirmation = (item) => {
    Alert.alert(
      strings.choseOption,
      item.taskName,
      [
        {
          text: strings.cancel,
          style: 'cancel',
        },
        {
          text: strings.undelete,
          onPress: () => {
            this.props.unDeleteTask(item.taskId)
          }
        },
        {
          text: strings.delete,
          onPress: () => {
            this.props.fullDeleteTask(item.taskId)
          }
        }

      ],
      {
        cancelable: true,
      },
    )
  }

  render() {
    const {tasks} = this.props

    return (
      <View style={styles.container}>
        {this.getTask(tasks).length > 0 ?
          <View>
            <View style={styles.timeline}/>
            <FlatList
              keyExtractor={(item) => item.taskId}
              style={styles.list}
              data={this.getTask(tasks)}
              renderItem={({item, index}) => (
                <Item
                  showPopup={this.showDialogConfirmation}
                  index={index}
                  item={item}
                />
              )}
            />
          </View>
          :
          <View style={styles.animation}>
            <Animation
              ref={animation => {
                this.animation = animation
              }}
              style={{
                width: metrics.screen_width / 2,
                height: metrics.screen_width / 2
              }}
              loop={false}
              onAnimationFinish={() => {
                setTimeout(() => {
                  this.animation.play()
                }, 1000)
              }}
              source={empty}
            />
            <Text style={styles.empty}>{strings.emptyTrash}</Text>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.colorBackground
  },
  animation: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  empty: {
    fontSize: 22,
    color: colors.colorTextTitle,
    marginTop: 20
  },
  list: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  },
  timeline: {
    height: '100%',
    width: 1,
    backgroundColor: colors.colorTimeline,
    marginLeft: 32
  }
})

const mapStateToProps = (state) => {
  return {
    tasks: state.todos.tasks
  }
}

const mapDispatchToProps = (dispatch) => ({
  unDeleteTask: id => {
    dispatch(unDeleteTask(id))
  },
  fullDeleteTask: id => {
    dispatch(fullDeleteTask(id))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoDoneScreen)