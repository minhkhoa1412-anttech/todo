import React, {Component} from 'react'
import {View, StyleSheet} from 'react-native'
import {getStatusBarHeight} from 'react-native-status-bar-height'
import ActionButton from 'react-native-action-button'
import {connect} from 'react-redux'

import AHeader from '../../components/AHeader/AHeader'
import AUserInformation from '../../components/AUserInformation/AUserInformation'
import TabNavigator from '../../navigator/AppNavigator'
import {colors} from '../../vars/colors'
import {platforms} from '../../vars/platforms'

class HomeScreen extends Component {
  constructor(props) {
    super(props)
  }

  getPercentCompleted = (tasks) => {
    let count = 0
    if (tasks.length > 0) {
      for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].taskDone) {
          count++
        }
      }
      return count / tasks.length
    } else {
      return 0
    }
  }

  getTask = (tasks) => (
    tasks.filter(task => (
      !task.taskDeleted
    ))
  )

  render() {
    const {tasks, navigation, isLoading} = this.props

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <AHeader/>
        </View>
        <View>
          <AUserInformation
            indeterminate={isLoading}
            progress={this.getPercentCompleted(this.getTask(tasks))}
            username={'thái minh khoa'}
            email={'thaiminhkhoa1412@gmail.com'}
            url={'https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/49783381_1032860776897619_948737981965926400_n.jpg?_nc_cat=111&_nc_ht=scontent.fsgn3-1.fna&oh=dd20f0f49b8828f4213a4d25d0c77827&oe=5CC42A86'}/>
        </View>
        <TabNavigator screenProps={navigation}/>
        <ActionButton
          onPress={() => {
            this.props.navigation.navigate('Modal')
          }}
          buttonColor={colors.colorAccent}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    height: platforms.isAndroid ? '8%' : '7%',
    marginTop: platforms.isAndroid ? 0 : getStatusBarHeight()
  }
})

const mapStateToProps = (state) => {
  return {
    tasks: state.todos.tasks,
    isLoading: state.todos.isLoading
  }
}

export default connect(mapStateToProps)(HomeScreen)