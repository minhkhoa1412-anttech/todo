import React, {Component} from 'react'
import {Text, View, TextInput, TouchableOpacity, StyleSheet} from 'react-native'
import Modalize from 'react-native-modalize'
import {connect} from 'react-redux'

import {metrics} from '../../vars/metrics'
import {strings} from '../../vars/strings'
import {colors} from '../../vars/colors'
import {addTask, editTask} from '../../actions/'

class ModalScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      taskName: this.props.edit.editTaskName
    }
  }

  modal = React.createRef()

  componentDidMount() {
    this.onOpenModal()
  }

  onOpenModal = () => {
    if (this.modal.current) {
      this.modal.current.open()
    }
  }

  onCloseModal = () => {
    this.setState({task: ''})
    this.props.navigation.goBack()
  }

  handlerEditOrAdd = (edit, taskName) => {
    if (edit.isEdit) {
      this.props.editTask(edit.editTaskId, taskName)
    } else {
      this.props.addTask(taskName)
    }
  }

  render() {
    const {edit} = this.props
    const {taskName} = this.state

    return (
      <Modalize
        onClosed={this.onCloseModal}
        height={metrics.screen_height * 30 / 100}
        ref={this.modal}
        handlePosition={'outside'}>

        <View style={styles.modal}>
          <Text style={styles.textModal}>{strings.addTask}</Text>
          <TextInput
            autoCorrect={false}
            autoFocus
            onChangeText={(text) => this.setState({taskName: text})}
            value={this.state.taskName}
            ref={this.textInputModal}
            style={styles.textInputModal}/>
          <TouchableOpacity
            onPress={() => {
              this.handlerEditOrAdd(edit, taskName)
              this.onCloseModal()
            }}
            style={styles.buttonSaveModal}>
            <Text style={{color: '#fff'}}>{strings.saveTask}</Text>
          </TouchableOpacity>
        </View>

      </Modalize>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    height: 210,
    padding: 30,
    justifyContent: 'space-between',
  },
  textModal: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.colorTextTitle
  },
  textInputModal: {
    padding: 5,
    marginVertical: 20,
    borderWidth: 2,
    borderColor: colors.colorAccent,
    borderRadius: 5,
  },
  buttonSaveModal: {
    padding: 10,
    backgroundColor: colors.colorAccent,
    borderRadius: 5,
    alignSelf: 'flex-end',
  },
})

const mapStateToProps = (state) => ({
  tasks: state.todos.tasks,
  edit: state.todos.editTask
})

const mapDispatchToProps = (dispatch) => ({
  addTask: (taskName) => {
    dispatch(addTask(taskName))
  },
  editTask: (taskId, taskName) => {
    dispatch(editTask(taskId, taskName))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalScreen)