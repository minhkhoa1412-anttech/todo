import React, {Component} from 'react'
import {Text, View, TextInput, TouchableOpacity, StyleSheet, Keyboard} from 'react-native'
import Modalize from 'react-native-modalize'
import {connect} from 'react-redux'

import {metrics} from '../../vars/metrics'
import {strings} from '../../vars/strings'
import {colors} from '../../vars/colors'
import {editTask} from '../../actions'

class AModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      task: ''
    }
  }

  modal = React.createRef()

  onOpenModal = () => {
    if (this.modal.current) {
      this.modal.current.open()
    }
  }

  onClosedModal = () => {
    if (this.modal.current) {
      this.modal.current.close()
    }
  }

  render() {
    const {addTask, editTask} = this.props
    const {task} = this.state

    return (
      <Modalize
        onClosed={() => {
          this.setState({task: ''})
        }}
        height={metrics.screen_height * 30 / 100}
        ref={this.modal}
        handlePosition={'outside'}>

        <View style={styles.modal}>
          <Text style={styles.textModal}>{strings.addTask}</Text>
          <TextInput
            autoCorrect={false}
            autoFocus
            onChangeText={(text) => this.setState({task: text})}
            value={this.state.task}
            ref={this.textInputModal}
            style={styles.textInputModal}/>
          <TouchableOpacity
            onPress={() => {
              addTask(task)
              this.onClosedModal()
            }}
            style={styles.buttonSaveModal}>
            <Text style={{color: '#fff'}}>{strings.saveTask}</Text>
          </TouchableOpacity>
        </View>

      </Modalize>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    height: 210,
    padding: 30,
    justifyContent: 'space-between',
  },
  textModal: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.colorTextTitle
  },
  textInputModal: {
    padding: 5,
    marginVertical: 20,
    borderWidth: 2,
    borderColor: colors.colorAccent,
    borderRadius: 5,
  },
  buttonSaveModal: {
    padding: 10,
    backgroundColor: colors.colorAccent,
    borderRadius: 5,
    alignSelf: 'flex-end',
  },
})

export default AModal