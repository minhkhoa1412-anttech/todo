import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {AAvatar} from '../AAvatar/AAvatar'
import {colors} from '../../vars/colors'
import {platforms} from '../../vars/platforms'

import AProgress from '../AProgress/AProgress'
import ATag from '../ATag/ATag'


class AUserInformation extends Component {
  render() {
    const {url, username, email, progress, indeterminate} = this.props

    return (
      <View style={styles.container}>
        <View style={styles.containProgress}>
          {!platforms.isAndroid ?
            <AProgress
              indeterminate={indeterminate}
              progress={progress}
              size={77}/> :
            <View style={{width: 100}}/>
          }
          <View style={styles.containAvatar}>
            <AAvatar
              size={70}
              url={url}/>
          </View>
        </View>
        <View style={styles.information}>
          <Text style={styles.textUsername}>{username.toUpperCase()}</Text>
          <ATag
            style={styles.viewTextEmail}
            label={email} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  containProgress: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  information: {
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  textUsername: {
    color: colors.colorTextTitle,
    fontSize: 20,
    fontWeight: 'bold'
  },
  viewTextEmail: {
    marginTop: 5,
  },
  containAvatar: {
    position: 'absolute'
  }
})

export default AUserInformation