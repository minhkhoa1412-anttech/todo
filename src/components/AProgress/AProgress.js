import React,{Component} from 'react'
import {View, StyleSheet} from 'react-native'
import * as Progress from 'react-native-progress'
import {colors} from '../../vars/colors'

class AProgress extends Component {
  render() {
    const {size, progress, indeterminate} = this.props

    return (
      <View>
        <Progress.Pie
          animated={true}
          unfilledColor= 'white'
          indeterminate={indeterminate}
          progress={progress}
          size={size} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {

  }
})

export default AProgress