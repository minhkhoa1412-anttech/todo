import * as React from 'react'
import {Image, StyleSheet} from 'react-native'

export const AAvatar = ({url, size}) => {
  return (
    <Image
      style={[
        styles.avatar,
        {
          width: size,
          height: size,
          borderRadius: size/2
        }
      ]}
      source={{uri: url}}/>
  )
}

const styles = StyleSheet.create({
  avatar: {
    backgroundColor: 'white',
    borderWidth: 2,
    borderColor: 'white'
  }
})
