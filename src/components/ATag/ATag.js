import React,{Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {colors} from '../../vars/colors'

class ATag extends Component {
  render() {
    const {label, style, styleLabel} = this.props

    return(
      <View style={[styles.viewTextEmail, style]}>
        <Text style={[styles.textEmail, styleLabel]}>{label}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewTextEmail: {
    backgroundColor: colors.colorAccentLighter,
    padding: 4,
    borderRadius: 3
  },
  textEmail: {
    color: colors.colorTextEmail
  }
})

export default ATag