import {constants} from '../vars/constants'

const initialState = {
  tasks: [],
  editTask: {
    isEdit: false,
    editTaskId: '',
    editTaskName: ''
  },
  errorMessenger: '',
  isLoading: false
}

export const todos = (state = initialState, action) => {
  switch (action.type) {
    case constants.FETCH_DATA:
      return {
        ...state,
        tasks: [],
        isLoading: action.isLoading
      }
    case constants.FETCH_DATA_SUCCESS:
      return {
        ...state,
        tasks: [
          action.payload,
          ...state.tasks
        ],
        isLoading: false
      }
    case constants.CHANGE_TASK_SUCCESS:
      return {
        ...state,
        tasks: state.tasks.map(
          task =>
            task.taskId === action.payload.taskId ? action.payload : task
        ),
        editTask: {
          isEdit: false,
          editTaskId: '',
          editTaskName: ''
        }
      }
    case constants.FULL_DELETE_TASK_SUCCESS:
      return {
        ...state,
        tasks: state.tasks.filter(
          task => task.taskId !== action.taskId
        )
      }
    case constants.PREPARE_EDIT:
      return {
        ...state,
        editTask: {
          isEdit: true,
          editTaskId: action.taskId,
          editTaskName: action.taskName
        }
      }
    default:
      return state
  }
}

export const trash = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}