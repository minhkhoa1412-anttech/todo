import {combineReducers} from 'redux'
import {todos,trash} from '../reducers/'

export default combineReducers({
  todos,
  trash
})
